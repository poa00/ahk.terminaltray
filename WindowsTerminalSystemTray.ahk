﻿; WindowsTerminalSystemTray.ahk
;
; Minimizes and restores Windows Terminal with a single keystroke.
; The terminal status can be toggled clicking on the System Tray icon as well.


#SingleInstance force
#WinActivateForce


; Initial setup.
SetupSystemTray()
ToggleWindowsTerminalStatusOnKeyboard()


; When the script terminates, the console is automatically closed.
OnExit("CloseConsole")


; <kbd>F7</kbd> toggles the Windows Terminal window status.
F7::
ToggleWindowsTerminalStatusOnKeyboard()
return


; System Tray icon and default action behavior.
reloadOrToggleWindowsTerminal:
ToggleWindowsTerminalStatusOnSystemTrayIcon()
return


; Sets up the System Tray to the active icon.
; It also defaults its behavior to the "Reload or toggle Windows Terminal" action, even clicking on the icon.
SetupSystemTray() {
    SetActiveIcon()

    Menu, Tray, add
    Menu, Tray, add, Reload or toggle Windows Terminal, reloadOrToggleWindowsTerminal
    Menu, Tray, Default, Reload or toggle Windows Terminal
    Menu, Tray, Click, 1
}

; Toggles the Visual Code window status according to certain rules.
; This function is used when the hotkey is pressed.
ToggleWindowsTerminalStatusOnKeyboard() {
    SetTitleMatchMode, 2
    DetectHiddenWindows, On

    if WinExist("ahk_exe WindowsTerminal.exe") {
        if WinActive("ahk_exe WindowsTerminal.exe") {
            WinMinimize
            SetInactiveIcon()
        } else {
            WinActivate
            SetActiveIcon()
        }
    } else {
        Run, "shell:AppsFolder\Microsoft.WindowsTerminal_8wekyb3d8bbwe!App", UseErrorLevel

        if (ErrorLevel = "ERROR") {
            SetInactiveIcon()
            Msgbox, "Windows Terminal not found!"
        } else {
            SetActiveIcon()
        }
    }
}


; Toggles the Visual Code window status according to certain rules.
; This functions is used when the System Tray icon is clicked.
; We need two different functions because clicking on the tray icon alters the active state of the windows.
; So, with this function, we can minimize or restore the terminal window, but we cannot activate it.
ToggleWindowsTerminalStatusOnSystemTrayIcon() {
    SetTitleMatchMode, 2
    DetectHiddenWindows, On

    if (WinExist("ahk_exe WindowsTerminal.exe")) {
        WinGet, s, MinMax, ahk_exe WindowsTerminal.exe

        switch s {
            ; Terminal is neither maximized nor minimized, so it's floating around.
            ; The window is minimized.
            case 0:
                WinMinimize
                SetInactiveIcon()
                return

            ; Terminal is maximized.
            ; The window is minimized.
            case 1:
                WinMinimize
                SetInactiveIcon()
                return

            ; Terminal is minimized.
            ; The window is restored to its former state.
            case -1:
                WinActivate
                SetActiveIcon()
                return

            default:
                return
        }
    } else {
        Run, "shell:AppsFolder\Microsoft.WindowsTerminal_8wekyb3d8bbwe!App", UseErrorLevel

        if (ErrorLevel = "ERROR") {
            SetInactiveIcon()
            Msgbox, "Windows Terminal not found!"
        } else {
            SetActiveIcon()
        }
    }
}


; Kills the console.
CloseConsole(){
    WinKill ahk_exe WindowsTerminal.exe
}


; Sets the inactive icon in the System Tray.
SetInactiveIcon() {
    Menu, Tray, Icon, WindowsTerminalHidden.ico
}


; Sets the active icon in the System Tray.
SetActiveIcon() {
    Menu, Tray, Icon, WindowsTerminalActive.ico
}